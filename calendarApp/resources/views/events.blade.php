@extends('layouts.app')
@section('title', 'Appetiser Apps : Calendar Code Challenge')

@section('content')

<!-- Begin::Main -->
<main>
    <div class="container">
        <div class="row">
            <div class="col col-xs-12 col-lg-4 my-5">
                <fieldset class="mt-5">    	
                    <legend>EVENT CALENDAR</legend>

                        @if(Session::has('success'))
                            <div class='alert alert-success alert-dismissible fade show' role='alert'> {{ Session::get('success') }}
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                    <span aria-hidden='true'>&times;</span>
                                </button>
                            </div>
                        @elseif(Session::has('warning'))
                            <div class='alert alert-warning alert-dismissible fade show' role='alert'> {{ Session::get('warning') }}
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                    <span aria-hidden='true'>&times;</span>
                                </button>
                            </div>
                        @endif

                        {{ Form::open(array('route' => 'events.add','method' => 'POST','files' => 'true')) }}

                            {{ Form::label('title','Event Title:', ['class' => 'font-weight-bold', 'style'=>'letter-spacing: 0px']) }}
                            {{ Form::text('title', null, ['class' => 'form-control mb-2']) }}

                            {{ Form::label('start','Start Date:', ['class' => 'font-weight-bold', 'style'=>'letter-spacing: 0px']) }}
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fas fa-calendar-alt"></i></div>
                                </div>
                                {{ Form::date('start', null, ['class' => 'form-control']) }}
                            </div>
                            
                            {{ Form::label('end','End Date:', ['class' => 'font-weight-bold', 'style'=>'letter-spacing: 0px']) }}
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fas fa-calendar-alt"></i></div>
                                </div>
                                {{ Form::date('end', null, ['class' => 'form-control']) }}
                            </div>

                            {{ Form::submit('Add Event', ['class'=>'btn btn-block btn-outline-primary', 'style'=>'font-size: 1rem']) }}

                        {{ Form::close() }}

                </fieldset>
            </div>  
            <div class="col col-xs-12 col-lg-8 my-5">
                <fieldset class="mt-5">    	
                    <legend>EVENT SCHEDULE</legend>
                    <div id="calendar"></div>
                </fieldset>
            </div>
        </div>
    </div>
</main>
<!-- End::Main -->

@endsection