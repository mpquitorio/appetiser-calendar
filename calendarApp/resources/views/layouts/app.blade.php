<!DOCTYPE html>
<!-- Begin::HTML -->
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <!-- Begin::Head -->
    <head>
        <!-- Meta -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="author" content="Michael P. Quitorio">
        <meta name="description" content="Appetiser Apps: Calendar Code Challenge using PHP Laravel Framework">

        <!-- Title -->
        <title>@yield('title')</title>

        <!-- Fontawesome -->
        <script src="https://kit.fontawesome.com/54dd48c648.js" crossorigin="anonymous"></script>
        <!-- Bootswatch -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <!-- jQuery -->
        <script src="{{ asset('js/jquery-3.3.1.min.js') }}" crossorigin="anonymous"></script>
        <!-- Bootstrap JS -->
        <script src="{{ asset('js/bootstrap.min.js') }}" crossorigin="anonymous"></script>
        <!-- FullCalendar -->
        <link href='{{ asset('css/fullcalendar.min.css') }}' rel='stylesheet' />
        <script src='{{ asset('js/moment.min.js') }}' crossorigin="anonymous"></script>
        <script src='{{ asset('js/fullcalendar.min.js') }}' crossorigin="anonymous"></script>

        <script>
            $(document).ready(function(){

                $('#calendar').fullCalendar({
                    header: {
                        left: 'prev, next',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay'
                    },
                    editable: false,
                    navLinks: true,
                    selectable: true,
                    events: JSON.parse('{!! $calendar !!}')
                });
            });
        </script>
    </head>
    <!-- End::Head -->
    <!-- Begin::Body -->
    <body>
        @include('includes.navbar')
        @yield('content')
        @include('includes.footer')
    </body>
    <!-- End::Body -->
</html>
<!-- End::HTML -->