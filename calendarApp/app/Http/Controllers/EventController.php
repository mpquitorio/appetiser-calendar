<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Response;
use Validator;
use App\Event;

class EventController extends Controller
{
    public function index(){

        $events = [];
        $datas = Event::get();

        if($datas->count()) {
            foreach ($datas as $key => $value) {
                $events[] = array(
                    'title' => $value->title,
                    'start' => $value->start,
                    'end'   => $value->end
                );
            }
            $calendar = json_encode($events);
        }else{
            $calendar = '';
        }
        return view('events')->with('calendar', $calendar);
    }
 
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'start' => 'required',
            'end'   => 'required'
        ]);

        if ($validator->fails()) {
        	\Session::flash('warning','Please enter details to proceed.');
            return Redirect::to('/')->withInput()->withErrors($validator);
        }

        $event = new Event;
        $event->title = $request['title'];
        $event->start = $request['start'];
        $event->end   = $request['end']."T23:59:00";
        $event->save();

        \Session::flash('success','Event added successfully.');
        return Redirect::to('/');
    }
}
